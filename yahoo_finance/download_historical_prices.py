#! /usr/bin/env python3

import subprocess
import argparse
import os

''' Script to download historical prices from yahoo finance.

Call it as
  <scriptname> -ticker <ticker>
'''
# Todo:- In future this api has to be extended to
#   <scriptname> -ticker <ticker> -start_date YYYYMMDD -end_date YYYYMMDD \
#     -odir <output_directory> -ofile <output_file>
# Todo:- If file already exists, do not overwrite it unless the --force option
# is specified.

def do_it(cmd, debug):
    if (debug):
        print(cmd)
    subprocess.call([cmd], shell=True)

def download_data(args):
    ticker = args.ticker
    debug = args.debug

    if debug:
        print("ticker = " + ticker)

    start_date = '20130718'
    end_date = '20150415'
    out_file = ticker + '_' + start_date + '_' + end_date + '.csv'

    if debug:
        print('out_file = ' + out_file)

    cmd = 'wget "http://real-chart.finance.yahoo.com/table.csv?s=' + ticker + '&d=3&e=15&f=2015&g=d&a=6&b=18&c=2013&ignore=.csv" -O ' + out_file
    if debug:
        print('cmd = ' + cmd)

    do_it(cmd, debug)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Download historical prices from yahoo finance')
    parser.add_argument(
        "ticker", action="store",
        help="ticker to download data")
    parser.add_argument(
        "--debug", action="store_true",
        default=False, dest="debug",
        help="show debug output")
    args = parser.parse_args()

    download_data(args)
